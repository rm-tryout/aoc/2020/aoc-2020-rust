use crate::{calucalte_from_vec, parse_input};

pub fn process_part_01(input: &str) -> isize {
  input
  .lines()
  .map(|l|{
    calucalte_from_vec(parse_input(l), false).0
  })
  .sum()

}