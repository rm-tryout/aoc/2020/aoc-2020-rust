use std::fmt::Debug;

pub mod part_01;
pub mod part_02;

#[derive(PartialEq, Debug)]
pub enum Operator {
  Plus,
  Mult
}


pub fn parse_input(input: &str) -> Vec<char> {
  input
  .chars()
  .filter(|&c| c != ' ')
  .collect()
}

pub fn process_operation (op: &Operator, current_sum: &isize, val: isize) -> isize {
  match op {
    Operator::Mult => current_sum * val,
    Operator::Plus => current_sum + val,
  }
}



pub fn calucalte_from_vec(input: Vec<char>, use_precendence: bool) -> (isize, Vec<char>) 
{
  // println!("opening new calc");
  let mut symbols = input.into_iter();

  let mut op = Operator::Plus;
  let mut sum = 0;

  while let Some(sym) = symbols.next() {
    match sym {
      '+' => {
        let last_operator = op;
        op = Operator::Plus;

        if use_precendence && last_operator == Operator::Mult {
          break;
        }
      },
      '*' => {
        op = Operator::Mult;
        if use_precendence {
          let val = calucalte_from_vec(symbols.collect(), use_precendence);
          sum = process_operation(&op, &sum, val.0);
          // println!("sym: {:?} op: {:?}, val: {:?} sum: {:?}", sym, op, val, sum);
          symbols = val.1.into_iter();
        }
      },
      '(' => {
        let val = calucalte_from_vec(symbols.collect(), use_precendence);
        sum = process_operation(&op, &sum, val.0);
        symbols = val.1.into_iter();
      },
      ')' => {
        break;
      },
      num_c=> {
        let val = num_c.to_digit(10).unwrap() as isize;
        sum = process_operation(&op, &sum, val);
        // println!("sym: {:?} op: {:?}, val: {:?} sum: {:?}", sym, op, val, sum);
      }
    }
  };

  // println!("closing calc {}", sum);
  (sum, symbols.collect())

}


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn calc_is_correct_01(){
    let test_input = "1 + 2 * 3 + 4 * 5 + 6";
    let result = calucalte_from_vec(parse_input(&test_input), false);

    assert_eq!(result.0, 71);
  }

  #[test]
  fn calc_is_correct_02(){
    let test_input = "1 + (2 * 3) + (4 * (5 + 6))";
    let result = calucalte_from_vec(parse_input(&test_input), false);

    assert_eq!(result.0, 51);
  }

  #[test]
  fn calc_for_precedence_01(){
    let test_input = "2 * 3 + (4 * 5)";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 46);
  }

  #[test]
  fn calc_for_precedence_02(){
    let test_input = "5 + (8 * 3 + 9 + 3 * 4 * 3)";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 1445);
  }

  #[test]
  fn calc_for_precedence_03(){
    let test_input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 23340);
  }

  #[test]
  fn calc_for_precedence_04(){
    let test_input = "((1)*(2+1)+2)";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 5);
  }

  #[test]
  fn calc_for_precedence_05(){
    let test_input = "1 + (2 * 3) + (4 * (5 + 6))";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 51);
  }

  #[test]
  fn calc_for_precedence_06(){
    let test_input = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 669060);
  }

  #[test]
  fn calc_for_precedence_07(){
    let test_input = "((1)*(2+1)+3*3*2)";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 36);
  }

  #[test]
  fn calc_for_precedence_08(){
    let test_input = "1 + 2 * 3 + 4 * 5 + 6";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 231);
  }

  #[test]
  fn calc_for_precedence_09(){
    let test_input = "2 * (8 + 6 * 2 * 6 + 2 * (6 + 5 + 9 + 6)) * 2 * 6";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 139776);
  }

  // TODO FIX!
  #[test]
  fn calc_for_precedence_12(){
    let test_input = "7+(2*6*4)*3";
    let result = calucalte_from_vec(parse_input(test_input), true);

    assert_eq!(result.0, 115);
  }
}


