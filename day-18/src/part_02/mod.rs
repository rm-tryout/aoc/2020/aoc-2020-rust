use crate::{parse_input, calucalte_from_vec};

pub fn process_part_02(input:&str) -> isize {
  input
  .lines()
  .map(|l|{
    calucalte_from_vec(parse_input(l), true).0
  })
  .sum()
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn works_with_test_input(){
    let test_input="1 + 2 * 3 + 4 * 5 + 6
1 + (2 * 3) + (4 * (5 + 6))
2 * 3 + (4 * 5)
5 + (8 * 3 + 9 + 3 * 4 * 3)
5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
";
    let result = process_part_02(test_input);
    assert_eq!(result, 694173)
  }
}