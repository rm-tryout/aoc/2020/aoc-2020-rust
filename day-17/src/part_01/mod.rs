use crate::apply_cycle;

pub fn process_part_01(input: &str) -> usize {
  let mut grid = crate::Grid::new(input,3);
  
  for _ in 0..6 {
    grid = apply_cycle(&grid);
  }

  grid.value.iter().filter(|(_, &active)| active).count()
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result, 112)
  }
}