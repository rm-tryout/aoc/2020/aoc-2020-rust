use std::collections::HashMap;

pub mod part_01;
pub mod part_02;

pub const TEST_INPUT:&str = ".#.
..#
###
";

#[derive(Clone)]
pub struct Grid {
  value: HashMap<Vec<isize>, bool>,
}
impl Grid {
  fn new(input: &str, dimensions: usize) -> Self {
    let value = input
    .lines()
    .enumerate()
    .flat_map(|(y, line)|{
      line
      .chars()
      .enumerate()
      .map( move |(x,c)|{
        let mut cube:Vec<isize> = vec![x as isize ,y as isize];
        for _ in 0..(dimensions -2) {
          cube.push(0)
        }
        (cube, c == '#')
      })
    }).collect::<HashMap<Vec<isize>, bool>>();

    return Self { value }
  }
}

pub fn get_neighbor_coords(input: &Vec<isize>) -> Vec<Vec<isize>> {
  let dimensions = input.clone().iter().len();
  let init:Vec<Vec<isize>> = vec![vec![]];
  let alterations = vec![-1,0,1];

  (0..dimensions)  
  .fold(init,|mut acc, _|{
    acc = acc
    .iter_mut()
    .flat_map(|v|{
      alterations
      .iter()
      .map(move |value| {
        let mut vec = v.clone();
        vec.push(*value);
        vec
      }).collect::<Vec<Vec<isize>>>()
    })
    .collect();

    acc
  })
  .into_iter()
  .filter(|v| !v.iter().all(|&f| f == 0))
  .map(|v|{
    v.into_iter().enumerate().map(|(ind, entry)|{
      entry + input[ind]
    }).collect()
  })
  .into_iter()
  .collect()

}

pub fn fill_grid_with_neighbours(input: &Grid) -> Grid{
  let mut grid = input.clone();
  
  grid.value= grid.value
  .clone()
  .iter_mut()
  .flat_map(|(coord, _)|{
    get_neighbor_coords(coord)
  })
  .fold(grid.value, |mut acc, coord|{
    acc.entry(coord).or_insert(false);
    acc
  });

  grid
}

pub fn apply_cycle( input: &Grid) -> Grid {
  let mut grid = fill_grid_with_neighbours(input);
  let original_grid = grid.clone();

  for (coord, active) in &original_grid.value {
    let n_coords = get_neighbor_coords(coord);
    let active_neighbors = 
      n_coords
      .iter()
      .fold(0 as usize, |acc, curr|{
        if *original_grid.value.get(curr).unwrap_or(&false) {
          acc + 1
        } else {
          acc
        }
      });

    match active {
      true => if active_neighbors != 2 && active_neighbors != 3 { grid.value.insert(coord.clone(), false); },
      false => if active_neighbors == 3 { grid.value.insert(coord.clone(), true); }
    }
  }

  grid.value = grid
  .value
  .into_iter()
  .filter(|(_, active)|{ *active })
  .collect();
  
  
  grid

}