use crate::{Grid, apply_cycle};

pub fn process_part_02(input:&str) -> usize {
  let mut grid = Grid::new(input, 4);
  
  for _ in 0..6 {
    grid = apply_cycle(&grid);
  }

  grid.value.iter().filter(|(_, &active)| active).count()
}

#[cfg(test)]
mod test {
  use crate::{get_neighbor_coords, TEST_INPUT};

use super::process_part_02;

  #[test]
  fn parses_neighbors(){
    let result = get_neighbor_coords(&vec![1,1,1]);
    assert_eq!(result.len(), 26);
  }

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 848);
  }
}