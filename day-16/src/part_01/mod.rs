use crate::{parse_input, get_invalid_number};

pub fn process_part_01(input: &str) -> usize{
  let (rules, _, nearby_tickets) =  parse_input(input);

  nearby_tickets
  .iter()
  .filter_map(|ticket|{
    get_invalid_number(ticket, rules.clone())
  })
  .sum()
}