use std::ops::RangeInclusive;

pub mod part_01;
pub mod part_02;

pub fn parse_input(input: &str) -> 
  (Vec<Vec<RangeInclusive<usize>>>, Vec<usize>, Vec<Vec<usize>>)
  {
  let mut lines = input.lines();
  
  let rules:Vec<Vec<RangeInclusive<usize>>> = lines
  .by_ref()
  .take_while(|&l| l != "")
  .map(|l| parse_line_to_rule(l))
  .collect();


  let my_ticket:Vec<usize> = lines
  .by_ref()
  .take_while(|&l| l != "")
  .last()
  .unwrap()
  .split(",")
  .map(|f|f.parse::<usize>().unwrap())
  .collect();

  
  let nearby_tickets:Vec<Vec<usize>> = lines
  .by_ref()
  .skip_while(|&l| !l.contains( "nearby tickets:") )
  .skip(1)
  .take_while(|&l| l != "")
  .map(|ticket|{
    ticket
    .split(",")
    .map(|f|f.parse::<usize>().unwrap())
    .collect()
  })
  .collect();

  (rules, my_ticket, nearby_tickets)
}

pub fn parse_line_to_rule(line: &str) -> Vec<RangeInclusive<usize>> {
  line
  .split(" ")
  .filter(|s| s.contains("-"))
  .map(|s|{
    let r:Vec<usize> = s
    .split("-")
    .map(|s| s.parse::<usize>().unwrap()
    ).collect();
    r[0]..=r[1]
  }).collect()
}

pub fn get_invalid_number(ticket: &Vec<usize>, rules: Vec<Vec<RangeInclusive<usize>>>) -> Option<usize> {
  for num in ticket {
    if rules
    .iter()
    .all(|rule|{
      !rule[0].contains(&num) && !rule[1].contains(&num)
    }) {
      return Some(*num)
    }
  }

  None
}

pub const TEST_INPUT:&str = "class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9";