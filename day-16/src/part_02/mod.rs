use std::collections::HashSet;

use crate::{parse_input, get_invalid_number};

pub fn process_part_02(input:&str) -> usize {
  let (rules, my_ticket, nearby_tickets) =  parse_input(input);  
  let field_count = &rules.len();
  let mut possible_fields = Vec::new();
  for original_rule_nr in 0..*field_count {
    possible_fields.push((original_rule_nr ,vec![true; *field_count]))
  }

  nearby_tickets
  .iter()
  .filter(|ticket|{
    get_invalid_number(ticket, rules.clone()).is_none()
  })
  .for_each(|ticket|{
    ticket.iter().enumerate().for_each(|(field_nr_ticket, num)|{
      rules.iter().enumerate().for_each(|(rule_nr, rule)|{
        if !rule[0].contains(num) && !rule[1].contains(num) {
          possible_fields[rule_nr].1[field_nr_ticket] = false
        }
      } )
    })
  });

  possible_fields
  .sort_by(|a,b|{
    a.1.iter().filter(|&&b| b ).count().cmp(
      &b.1.iter().filter(|&b|*b).count()
    )
  });

  let mut already_used:HashSet<usize> = HashSet::new();

  possible_fields
  .iter()
  .map(|(rule_index, possible_fields)|{
    possible_fields
    .iter()
    .enumerate()
    .filter_map(|(field_index,possible)| 
      if *possible { Some((*rule_index,field_index)) } else { None })
    .collect::<Vec<(usize, usize)>>()
  })
  .map(| d |{
    let num = d
      .iter()
      .find(|&num| { 
        !already_used.contains(&num.1) }
      ).unwrap();

    already_used.insert(num.1);
    *num
  })
  .fold(1, |acc,cur|{
    if (0..6).contains(&cur.0){
      return acc*my_ticket[cur.1]
    }
    acc
  })
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 132);
  }
}