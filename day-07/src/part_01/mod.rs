use std::collections::HashMap;

use crate::{Bag, can_hold};

pub fn process_part_01(input: &str) -> usize {
  
  let all_bags = input
  .lines()
  .map(|line|{
    Bag::from_str(line)
  })
  .fold(HashMap::new(), |mut acc, cur|{
    acc.insert(cur.color, cur);
    acc
  });

  all_bags.clone().into_iter()
  .filter(|(color, _)|{
    can_hold(("shiny", "gold"), *color, &all_bags)
  })
  .count()
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result, 4);
  }
}