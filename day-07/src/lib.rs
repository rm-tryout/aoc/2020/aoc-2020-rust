pub mod part_01;
pub mod part_02;

use std::collections::HashMap;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct Bag<'a> {
  color: (&'a str, &'a str),
  contents: HashMap<(&'a str, &'a str), usize>
}

impl<'a> Bag<'a> {
  fn from_str(line:&'a str) -> Self {
    let info = line.split(" bags contain ").collect::<Vec<&'a str>>();
    let mut color = info[0].split(" ");
    let mut bag = Bag{ color: (color.next().unwrap(), color.next().unwrap()) , contents: HashMap::new()};
    
    if info[1].contains("no other bags") {
      return bag;
    }

    info[1]
    .split(", ")
    .for_each(|child_info|{
      let mut words = child_info.split(" ");
      let count = words.next().unwrap().parse::<usize>().unwrap();
      
      bag.contents.insert((words.next().unwrap(), words.next().unwrap()),count);
    });

    bag
  }
}

pub fn can_hold(target: (&str, &str), color: (&str, &str), list: &HashMap<(&str, &str), Bag>) -> bool{
  let bag = list.get(&color).unwrap();
  if bag.contents.is_empty() {
    return false;
  }

  if bag.contents.contains_key(&target){
    return true;
  }

  bag.contents.iter().any(|child|{
    can_hold(target, *child.0, list)
  })
}

pub const TEST_INPUT:&str = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn can_parse_bag(){
    let line = "light red bags contain 1 bright white bag, 2 muted yellow bags";
    let result = Bag::from_str(line);
    

    let mut contents = HashMap::new();
    contents.insert(("bright", "white"), 1);
    contents.insert(("muted", "yellow"), 2);

    assert_eq!(result, Bag{ 
      color: ("light", "red"), 
      contents
    });
  }
}