use std::collections::HashMap;

use crate::Bag;

fn count_all_children(bag: &(&str, &str), list: &HashMap<(&str, &str), Bag>) -> usize{
  list
  .get(&bag)
  .unwrap()
  .contents
  .iter()
  .fold(0 as usize,|acc, (color, count)|{
    acc + count + count * count_all_children(color, list)
  })
}

pub fn process_part_02(input:&str) -> usize{
  let all_bags = input
  .lines()
  .map(|line|{
    Bag::from_str(line)
  })
  .fold(HashMap::new(), |mut acc, cur|{
    acc.insert(cur.color, cur);
    acc
  });

  count_all_children(&("shiny", "gold"), &all_bags)
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn works_with_input(){
    let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
    let result = process_part_02(input);

    assert_eq!(result, 126);
  }

}