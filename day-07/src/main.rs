use std::{fs, time::Instant};

use day_07::part_01::process_part_01;
use day_07::part_02::process_part_02;

fn main(){
  let input = fs::read_to_string("./input.txt").unwrap();
  let start_01 = Instant::now();
  let result_01 = process_part_01(&input);
  println!("
  Result of part 1:   {:?}
  Execution time was: {:?}
  ", result_01, Instant::now() - start_01);

  let start_02 = Instant::now();
  let result_02 = process_part_02(&input);
  println!("
  Result of part 2:   {:?}
  Execution time was: {:?}
  ", result_02, Instant::now() - start_02);
}