#![allow(unused)]

pub fn process_part_01(input: &str) -> usize {
  input
  .split("\n\n")
  .map(|l|{
    l
    .split(" ")
    .flat_map(|item|item.split("\n"))
    .map(|e|{ 
      e.split(":")
      .next()
      .unwrap()
    })
    .collect::<Vec<&str>>()
  })
  .filter(|pp|{
    vec![
      "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"
    ].iter().all(|f|{pp.contains(f)})
  })
  .count()
}
#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result, 2);
  }
}