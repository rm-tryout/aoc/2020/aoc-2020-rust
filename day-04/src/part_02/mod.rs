pub fn process_part_02(input:&str) -> usize{
  input
  .split("\n\n")
  .map(|l|{
    l
    .split(" ")
    .flat_map(|item|item.split("\n"))
    .filter(|&i| i != "")
    .map(|e|{ 
      e.split(":").collect::<Vec<&str>>()
    })
    .collect::<Vec<Vec<&str>>>()
  })
  .filter(|pp|{
    pp
    .iter()
    .all(|field|{
      let result = match field[0] {
        "byr" =>  field[1].len() == 4 && 
                  field[1].parse::<usize>().unwrap() >= 1920 && 
                  field[1].parse::<usize>().unwrap() <= 2002,
        "iyr" =>  field[1].len() == 4 &&
                  field[1].parse::<usize>().unwrap() >= 2010 && 
                  field[1].parse::<usize>().unwrap() <= 2020,
        "eyr" =>  field[1].len() == 4 &&
                  field[1].parse::<usize>().unwrap() >= 2020 &&
                  field[1].parse::<usize>().unwrap() <= 2030,
        "hgt" =>  {
          if let Ok(height) = field[1][..field[1].len()-2].parse::<usize>(){
            match field[1].chars().rev().take(2).collect::<String>().chars().rev().collect::<String>().as_str(){
              "cm" => height >= 150 && height <= 193,
              "in" => height >= 59 && height <= 76,
              _ => false
            }
          } else {
            false
          }
        },
        "hcl" =>  field[1].len() == 7 &&
                  field[1].chars().next().unwrap() == '#' &&
                  field[1][1..7].chars().all(|c| c.is_ascii_hexdigit()),
        "ecl" => vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&field[1]),
        "pid" => field[1].len() == 9 && field[1].parse::<usize>().is_ok(),
        "cid" => true,
        _ => false
      };


      let fields_contained = pp.iter()
      .map(|entry| entry[0])
      .collect::<Vec<&str>>();

      let all_fields_present = vec![
        "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"
      ].iter().all(|f|{fields_contained.contains(f)});
      

      result && all_fields_present


    })

  })
  .count()
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input_invalid(){
    let test_input = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";

    let result = process_part_02(test_input);
    assert_eq!(result, 0)
  }

  #[test]
  fn works_with_test_input_valid(){
    let test_input = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";

    let result = process_part_02(test_input);
    assert_eq!(result, 4)
  }
}