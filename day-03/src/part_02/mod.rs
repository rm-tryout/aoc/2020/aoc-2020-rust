pub fn process_part_02(input:&str) -> usize {
  let coords:Vec<(usize,usize)> = vec![
    (1,1),
    (3,1),
    (5,1),
    (7,1),
    (1,2)
  ];

  coords
    .iter()
    .map(|coord|{
      input
      .lines()      
      .step_by(coord.1)
      .enumerate()
      .filter(|(y,l)|{
        l.chars().nth(y*coord.0%l.len()).unwrap() == '#'
      })
      .count()
  })
  .fold(1, |acc, cur| acc * cur)
}


#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 336);
  }
}

