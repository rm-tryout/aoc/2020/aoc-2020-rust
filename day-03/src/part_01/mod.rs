pub fn process_part_01(input: &str) -> usize{
  input
  .lines()
  .enumerate()
  .skip(1)
  .filter(|(y, l)|{
    l.chars().nth(y*3%l.len()).unwrap() == '#'
  })
  .count()
}


#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result, 7);
  }
}