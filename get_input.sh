#!/usr/bin/env bash

BASEDIR=$(dirname $0);

COOKIE=$(cat $BASEDIR/creds/cookie.txt)

DAY=$1
DAY_PREFIXED=$(printf '%02d\n' "$DAY")
YEAR=${2:-2020}

DIR=${BASEDIR}/day-${DAY_PREFIXED}


if [[ ! -d $DIR ]]; then
  echo "Day not present yet. Creating..."
  CURRENT_DIR=$(pwd)
  cd $BASEDIR
  cargo new --lib day-${DAY_PREFIXED}
  cd $CURRENT_DIR
  echo "Done creating cargo"
fi

echo "Downloading input for day $DAY/$YEAR and saving to $DIR/input.txt ..."
curl -sL -H "Cookie:${COOKIE}" https://adventofcode.com/${YEAR}/day/${DAY}/input > $DIR/input.txt
echo Done
