pub fn process_part_02(input:&str) -> usize {
  input
  .lines()
  .filter(|l|{
    let parts = l.split(" ").collect::<Vec<&str>>();
    let first_second = parts[0].split("-").map(|s|{s.parse::<usize>().unwrap()}).collect::<Vec<usize>>();
    let first = first_second[0] -1;
    let second = first_second[1] -1;
    let letter = parts[1].chars().next().unwrap().clone();

    let occs = vec![
      parts[2]
        .chars()
        .nth(first)
        .unwrap(),
      parts[2]
        .chars()
        .nth(second)
        .unwrap()
    ].iter()
    .filter(|&&c| c == letter)
    .count();
    

    occs == 1
  })
  .count()
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 1);
  }
}