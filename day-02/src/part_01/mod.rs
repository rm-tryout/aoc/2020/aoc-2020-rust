#![allow(unused)]

pub fn process_part_01(input: &str) -> usize{
  
  input
  .lines()
  .filter(|l|{
    let parts = l.split(" ").collect::<Vec<&str>>();
    let from_to = parts[0].split("-").map(|s|{s.parse::<usize>().unwrap()}).collect::<Vec<usize>>();
    let from = from_to[0];
    let to = from_to[1];
    let letter = parts[1].chars().next().unwrap().clone();

    let occs = parts[2]
    .chars()
    .filter(|&c| letter == c)
    .count();

    occs >= from && occs <= to
  })
  .count()
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result, 2);
  }
}