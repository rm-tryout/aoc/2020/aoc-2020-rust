use std::collections::HashMap;

pub fn process_part_01(input: &str, nr: usize) -> usize {

  let mut results : HashMap<usize, Vec<Option<usize>>> = HashMap::new();
  let mut starting_numbers = input
    .lines()
    .next()
    .unwrap()
    .split(",")
    .map(|l| l.parse::<usize>().unwrap() );
  
  let mut last_spoken = 0;

  for i in 0 as usize..nr {
    if let Some (val) = starting_numbers.next() {
      results.insert(val, vec![Some(i),None]);
      last_spoken = val;
      continue;
    }

    match results.get(&last_spoken) {
      Some(val) => match (val[0], val[1]) {
        (Some(_), None) => {
          last_spoken = 0;
        },

        (Some(u), Some(v)) => {
          last_spoken = u-v;
        },
        _ => panic!("None None should never happen")
      },
      None => {
        last_spoken = 0;
      }
    }

    match results.get(&last_spoken) {
      Some(val) => {
        results.insert(last_spoken, vec![Some(i), val[0]]);
      },
      None=> {
        results.insert(last_spoken, vec![Some(i), None]);
      }
    }
  }

  

  last_spoken
}


#[cfg(test)]
mod test {
  use super::*;

  
  #[test]
  fn parses_starting_numbers_correctly(){
    let result = process_part_01("0,3,6", 2020);
    assert_eq!(result, 436);
  }
}