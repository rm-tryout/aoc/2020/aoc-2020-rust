pub mod part_01;
pub mod part_02;

pub const TEST_INPUT:&str = "abc

a
b
c

ab
ac

a
a
a
a

b";