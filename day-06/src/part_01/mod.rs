use std::collections::HashMap;

pub fn process_part_01(input: &str) -> usize{

  input
  .split("\n\n")
  .map(|group|{
    group
    .chars()
    .filter(|c|c.is_alphabetic())
    .fold(HashMap::new(), |mut acc, c| {
      *acc.entry(c).or_insert(0) += 1;
      acc
    })
    .iter()
    .count()
  })
  .sum()
  
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result, 11);
  }
}