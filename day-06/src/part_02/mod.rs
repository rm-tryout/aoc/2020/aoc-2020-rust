use std::collections::{HashMap, HashSet};

pub fn process_part_02(input:&str) -> usize {

  input
  .split("\n\n")
  .map(|group|{
    let group_size = group.lines().count();

    group
    .lines()
    .fold(HashMap::new(), |mut acc, line|{
      let unique_line = line.chars().collect::<HashSet<char>>().into_iter().collect::<String>();
      unique_line.chars().for_each(|c|{
        *acc.entry(c).or_insert(0) += 1;
      });
      acc
    })
    .iter()
    .filter(|(_c, &occ)|{ occ == group_size})
    .count()
  })
  .sum()
}


#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 6);
  }
}