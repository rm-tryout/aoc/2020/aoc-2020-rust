use std::collections::HashMap;

use crate::get_row_and_column;

#[allow(dead_code, unused)]

pub fn process_part_02(input:&str) -> usize {
  let mut available_seats: HashMap<(usize, usize), usize> = HashMap::new();
  for r in 0..128 {
    for c in 0..8 {
      // if r*8+c != -1 && r*8+c != 1 {
        available_seats.insert((r,c), r*8+c);
      // }
      
    }
  }

  input
  .lines()
  .map(|l|get_row_and_column(l))
  .for_each(|coord|{
    available_seats.remove(&coord);
  });

  let mut ids = available_seats
  .iter()
  .map(|((r,c),_)|{
    r*8+c
  })
  .collect::<Vec<usize>>();
  
  ids.sort();

  let mut l_id = ids.first().unwrap().to_owned();
  let mut h_id = ids.last().unwrap().to_owned();
  
  while ids.contains(&h_id){
    h_id -= 1;
    ids.pop();
  }

  ids.reverse();
  while ids.contains(&l_id){
    l_id += 1;
    ids.pop();
  }

  ids[0]

}