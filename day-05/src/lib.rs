pub mod part_01;
pub mod part_02;

pub fn parse_seat_instruction(input:&str, upper_char: char) -> usize {
  let length = input.len();
  input
    [..length]
    .chars()
    .enumerate()
    .fold(0, |acc,(ind, c)|{
      let power = length - 1 - ind;
      if c == upper_char {
        acc + (2 as usize).pow(power as u32)
      } else {
        acc
      }
    })
}

pub fn get_row_and_column(input:&str) -> (usize, usize){
  (
    parse_seat_instruction(&input[..7], 'B'),
    parse_seat_instruction(&input[7..], 'R')
  )
}


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn can_parse_seat_instruction_row(){
    let input = "FBFBBFF";
    let result = parse_seat_instruction(input, 'B');

    assert_eq!(result, 44)
  }

  #[test]
  fn can_parse_seat_instruction_column(){
    let input = "RLR";
    let result = parse_seat_instruction(input, 'R');

    assert_eq!(result, 5)
  }

  #[test]
  fn can_get_row_and_column(){
    let input = "FBFBBFFRLR";
    let result = get_row_and_column(input);

    assert_eq!(result, (44,5));
  }
}
