use crate::get_row_and_column;

pub fn process_part_01(input: &str) -> usize{
  input
  .lines()
  .map(|l| get_row_and_column(l))
  .map(|(r,c)|{ r*8+c})
  .max()
  .unwrap()
}