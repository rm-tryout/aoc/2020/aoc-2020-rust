#![allow(dead_code)]

pub mod part_01;
pub mod part_02;

const TEST_INPUT:&str = "1721
979
366
299
675
1456";