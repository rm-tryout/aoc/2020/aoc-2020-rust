pub fn process_part_01(input: &str) -> usize {

  let input = input.lines().map(|l|{l.parse::<usize>().unwrap()});

  for i1 in input.clone() {
    if i1 < 2020 {
      for i2 in input.clone() {
        if i1 + i2 == 2020 {
          return i1*i2;
        }
      }
    }
  }

  0
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result, 514579);
  }
}