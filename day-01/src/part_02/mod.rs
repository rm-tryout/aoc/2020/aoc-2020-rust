
#![allow(unused)]

use itertools::Itertools;

pub fn process_part_02(input:&str) -> usize {
  let input = input.lines().map(|l|{l.parse::<usize>().unwrap()});  
  
  // Prettier but sadly also much slower
  // TODO: find a way to include the filtering
  
  // input
  // .clone()
  // .filter(|&v|{ v<= 2020})
  // .combinations(3)
  // .find(|i| i.iter().sum::<usize>() == 2020 )
  // .unwrap()
  // .iter()
  // .fold(1, |acc,&cur| acc * cur )
  

  // Ugly but fast
  for i1 in input.clone() {
    if i1 < 2020 {
      for i2 in input.clone() {
        if i1 + i2 < 2020 {
          for i3 in input.clone() {
            if i1 + i2 + i3 == 2020 {
              return i1*i2*i3;
            }
          }
        }
      }
    }
  }

  0
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 241861950);
  }
}