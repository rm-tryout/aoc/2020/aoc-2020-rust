use std::collections::HashSet;
use crate::{InstructionSet, get_instrution_set, toggle_nth_jmp_or_nop};

pub fn process_part_02(
  input: &str
) -> isize {
  let input = get_instrution_set(input);  
  let mut tries = 0;

  loop {

    if let Some (acc) = terminates_with(
      &toggle_nth_jmp_or_nop(&input, tries)) {
      return acc
    } else {
      tries += 1;
    }
  }
}

pub fn terminates_with(input: &InstructionSet) -> Option<isize> {

  let instruction_set = input.clone();
  let mut executed_before: HashSet<usize> = HashSet::new();
  let mut acc = 0;
  let mut index = 0;
  
  
  loop {
    let result = instruction_set.clone().nth(index);
    if result == None {
      return Some(acc);
    }
    
    let (i_nr, inst, arg) = result.unwrap();

    if executed_before.contains(&i_nr) {
      return None;
    } else {
      executed_before.insert(i_nr);
    }
        
    match inst.as_ref() {
      "nop" => {
        index += 1 ;
      },
      "acc" => {
        acc += arg;
        index += 1;
      },
      "jmp" => {
        index = (index as isize + arg) as usize;
      }
      _ => panic!("unknown instruction")
    }
  }
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 8);
  }
}
