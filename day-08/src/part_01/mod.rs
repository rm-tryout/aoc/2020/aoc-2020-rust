use std::collections::HashSet;

use crate::get_instrution_set;

pub fn process_part_01(input: &str) -> isize {
  let input = get_instrution_set(input);
  
  let mut acc = 0 as isize;
  let mut executed_before:HashSet<usize> = HashSet::new();
  let mut index = 0;

  loop {
    let (i_nr, inst, arg) = input.clone().nth(index).unwrap();
    if executed_before.contains(&i_nr) {
      break;
    } else {
      executed_before.insert(i_nr);
    }

    match inst.as_str() {
      "nop" => {
        index +=1;
      },
      "acc" => {
        acc += arg;
        index += 1;
      },
      "jmp" => {
        index = (index as isize + arg) as usize;
      }
      _ => panic!("unknown instruction")
    }
  }

  acc
  
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    
    assert_eq!(result, 5)
  }
}