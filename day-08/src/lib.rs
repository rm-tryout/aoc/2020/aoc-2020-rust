use std::vec::IntoIter;

pub mod part_01;
pub mod part_02;

pub type InstructionSet = IntoIter<(usize, String, isize)>;

pub fn toggle_nth_jmp_or_nop(input: &InstructionSet, n: usize) -> InstructionSet{
  let mut cloned_input = input
  .clone()
  .into_iter()
  .collect::<Vec<(usize, String, isize)>>();

  let first_nop_or_jmp = cloned_input  
  .iter_mut()
  .filter(|a |{
    *a == &(a.0, "nop".to_string(), a.2) || 
    *a == &(a.0, "jmp".to_string(), a.2)
  })
  .nth(n)
  .unwrap();

  first_nop_or_jmp.1 = if first_nop_or_jmp.1.as_str() == "nop" {
    "jmp".to_string()
  } else {
    "nop".to_string()
  };

  cloned_input.into_iter()

}

pub fn get_instrution_set(input: &str) -> InstructionSet {
  input
  .lines()
  .enumerate()
  .map(|(i_nr, l)| {
    let mut l_it = l.split(" ");    
    (
      i_nr, 
      l_it.next().unwrap().to_string(), 
      l_it.next().unwrap().parse::<isize>().unwrap()
    )
  })
  .collect::<Vec<(usize,String, isize)>>()
  .into_iter()
}

pub const TEST_INPUT:&str = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn switch_first_jmp_or_nop_works(){
    // Prepare
    let set = get_instrution_set(TEST_INPUT);
    let result = 
    toggle_nth_jmp_or_nop(&set, 0)
    .collect::<Vec<(usize, String, isize)>>();

    assert_eq!(result[0], (0, "jmp".to_string(), 0));
  }

    #[test]
    fn switch_first_jmp_or_nop_works_2(){
    // Prepare
    let set = get_instrution_set(TEST_INPUT);
    let result = 
    toggle_nth_jmp_or_nop(&set, 2)
    .collect::<Vec<(usize, String, isize)>>();

    assert_eq!(result[0], (0, "nop".to_string(), 0));
    assert_eq!(result[4], (4, "nop".to_string(), -3));

  }
}