use std::{fs, time::Instant};

use day_09::part_01::process_part_01;
use day_09::part_02::process_part_02;
// use crate{part_01, part_2};

fn main(){
  let input = fs::read_to_string("./input.txt").unwrap();
  let start_01 = Instant::now();
  let result_01 = process_part_01(&input, 25);
  println!("
  Result of part 1:   {:?}
  Execution time was: {:?}
  ", result_01, Instant::now() - start_01);

  let start_02 = Instant::now();
  let result_02 = process_part_02(&input, 25);
  println!("
  Result of part 2:   {:?}
  Execution time was: {:?}
  ", result_02, Instant::now() - start_02);
}