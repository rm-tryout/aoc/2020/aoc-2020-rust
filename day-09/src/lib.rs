use std::collections::HashSet;

pub mod part_01;
pub mod part_02;

struct Nums {
  val: Vec<usize>,
  curr: usize,
  preamble_size: usize
}

impl Nums {
  fn new(input: &str, preamble_size: usize) -> Self {
    Self {
      val: input.lines().map(|l| l.parse::<usize>().unwrap()).collect(),
      curr: preamble_size,
      preamble_size: preamble_size
    }
  }

  fn find_sequence_for_sum(&self, sum:usize) -> Vec<usize>{

    for i in 0..self.val.len() {

      let mut current_sum = 0;

      let collected:Vec<usize> = self
      .val
      .iter()
      .skip(i)
      .take_while(|&num|{
        current_sum += num;
        current_sum < sum
      })
      .map(|r|{ *r })
      .collect();

      if current_sum == sum {
        return collected;
      } else {
        continue;
      }
    }

    panic!("sequence could not be determined");
  }
}

impl Iterator for Nums {
  type Item = (usize, bool);
  
  fn next(&mut self) -> Option<Self::Item> {
      let valids = process_preamble(
        &self.val
        .clone()
        .into_iter()
        .skip(self.curr-self.preamble_size)        
        .take(self.preamble_size)
        .collect()
      );

      let result = if let Some(val) = self.val.get(self.curr) {
        Some((*val, valids.contains(val)))
      } else {
        None
      };

      self.curr += 1;

      result
  }
}

pub fn process_preamble(input: &Vec<usize>) -> HashSet<usize> {
  let mut result = HashSet::new();

  input
  .iter()
  .for_each(|x|{
    input
    .iter()
    .for_each(|y|{
      if x != y{
        result.insert(x+y);
      }
    })
  });

  result
}



pub const TEST_INPUT:&str = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";