use crate::{Nums, part_01::process_part_01};

pub fn process_part_02(input:&str, preamble_length:usize) -> usize {
  let sum = process_part_01(input, preamble_length);
  let nums = Nums::new(input, preamble_length);

  let seq = nums
  .find_sequence_for_sum(sum);

  seq.iter().max().unwrap() + seq.iter().min().unwrap()
  
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_02(TEST_INPUT, 5);
    assert_eq!(result, 62);
  }
}