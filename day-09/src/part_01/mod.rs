use crate::Nums;

pub fn process_part_01(input: &str, preamble_length: usize) -> usize {
  let nums = Nums::new(input, preamble_length);

  for (num, valid) in nums {
    if !valid {
      return num
    }
  }

  panic!("could not find invalid number");
}


#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn works_with_test_input(){
    let result = process_part_01(TEST_INPUT, 5);
    assert_eq!(result, 127);
  }
}